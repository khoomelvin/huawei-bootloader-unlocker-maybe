#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Compile the program not only with gcc stock optimizations, but also with
 * gcc -o brute -flto -march=native -pipe -faggressive-loop-optimizations dumb_brute_force.c
 */
int main()
{
	/*
	 * Declairing Vars
     *
	 * android bootloader unlock codes are always 16 digits.
	 */
    

    char starts[] = {"00","01","02","03","04","05","06","07","08","09"};
    for ( int i = 10, char str[2]; i < 100; i++ ){
        sprintf(str, "%i", i);
        starts[i] = str;
    }

	int pos[] = {00,10,01,00,10,01,00,10};
	int comp[] = {99,99,99,99,99,99,99,99};
    char fou[] = "fastboot oem unlock ", command[strlen(fou) + 16 + 14];
    char Final[16];
    int bool0 = 1;

	for ( int i = 0, state = 1; (state != 0 && pos!=comp); i++ ){

        bool0 = 1;           
        for (int i = 7; i !=-1 ; i--){
            if (pos[i] == 100){
                pos[i] = 0;
                pos[i-1] += 1;

            }
        }
        for (int i = 1; i < 8; i++){
            if ( ( starts[pos[i-1]][0] == starts[pos[i-1]][1] ) 
              && ( starts[pos[i-1]][0] == starts[pos[i]][0] ) ){
                bool0 = 0;
                break;   
            }
            if ( ( starts[pos[i-1]][0] != starts[pos[i-1]][1] ) 
              && ( starts[pos[i-1]][1] == starts[pos[i]][0] ) 
              && ( starts[pos[i]][0] == starts[pos[i]][1] ) ){
                bool0 = 0;
                break;
            }
        }


        if (bool0){
            
            Final = "";

            for (int i = 0; i < 8 ; i++){
                strcat(Final, starts[pos[i]]);
            }
            sprintf(command, "%s%s 2> /dev/null", fou, Final);

            //print only 1000 perms at a time since printf is a time
		    //demanding function
		    if (i == 1000){
			    i = 0;
			    printf("%s\n", command);
		    }
		    state = system(command);
        }
        pos[5] += 1;
	}

	printf("\n\nYour unlock code is: %s\n\n", Final);
	return 0;
}
